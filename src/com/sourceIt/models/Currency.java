package com.sourceIt.models;

public class Currency {
    private String name;
    private float rateOfPurchase;
    private float rateOfSale;

    public Currency(String name, float rateOfPurchase, float getRateOfSales) {
        this.name = name;
        this.rateOfPurchase = rateOfPurchase;
        this.rateOfSale = getRateOfSales;
    }

    public String getName() {
        return name;
    }

    public float getRateOfPurchase() {
        return rateOfPurchase;
    }

    public float getRateOfSale() {
        return rateOfSale;
    }
}
