package com.sourceIt;

import com.sourceIt.models.financialLogistics.FinanсeManager;


public class Main {

    public static void main(String[] args) {

        int amount = 20000;
        String desiredCurrencyName = "eur";
        String desiredOperationWithCurrency = "купить";
        int depositPeriod = 9;

        FinanсeManager.findAndPrintBestOrganizationToChangeMoney(amount,desiredCurrencyName,desiredOperationWithCurrency);
        FinanсeManager.findAndPrintBestOrganizationToCredit(amount);
        FinanсeManager.findAndPrintBestOrganizationToDeposit(amount,depositPeriod);
        FinanсeManager.findAndPrintBestOrganizationToSendMoney(amount);
    }
}