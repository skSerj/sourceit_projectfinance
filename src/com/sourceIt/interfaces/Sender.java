package com.sourceIt.interfaces;

public interface Sender {
    float sendMoney(int amount);
}
