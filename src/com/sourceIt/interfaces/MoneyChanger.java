package com.sourceIt.interfaces;

public interface MoneyChanger {
    String buyCurrency = "купить";
    String sellCurrency = "продать";

    float convertMoney(int amount, String desiredCurrencyName, String desiredOperationWithCurrency);
    }
