
package com.sourceIt.models.financialLogistics;

import com.sourceIt.data.Generator;
import com.sourceIt.interfaces.Creditor;
import com.sourceIt.interfaces.Deposite;
import com.sourceIt.interfaces.MoneyChanger;
import com.sourceIt.interfaces.Sender;

import java.util.ArrayList;
import java.util.List;

public class FinanсeManager {

    // обмен
    public static void findAndPrintBestOrganizationToChangeMoney(int amount, String desiredCurrencyName, String desiredOperationWithCurrency) {
        List<FinanceOrganization> financeOrganizations = new Generator().generate();
        float amountAfterPurchaseCurrency = 0;
        FinanceOrganization cheapestOrganizationToChange = null;
        for (FinanceOrganization financeOrganization : financeOrganizations) {
            if (financeOrganization instanceof MoneyChanger) {
                MoneyChanger moneyChanger = (MoneyChanger) financeOrganization;
                float moneyAfterChange = moneyChanger.convertMoney(amount, desiredCurrencyName, desiredOperationWithCurrency);
//                System.out.println(String.format("ваши деньги: %.2f", moneyAfterChange) + " " + financeOrganization.toString());
                if (moneyAfterChange > amountAfterPurchaseCurrency) {
                    amountAfterPurchaseCurrency = moneyAfterChange;
                    cheapestOrganizationToChange = financeOrganization;
                }
            }
        }
        if (cheapestOrganizationToChange != null)
            System.out.println(String.format("%s. После конвертации в наиболее выгодной организации, вы получите: %.2f ", cheapestOrganizationToChange.toString(), amountAfterPurchaseCurrency));
    }

    // кредит
    public static void findAndPrintBestOrganizationToCredit(int amount) {
        List<FinanceOrganization> financeOrganizations = new Generator().generate();
        float loanInterest = Float.MAX_VALUE;
        FinanceOrganization bestOrganizationToCredit = null;
        for (FinanceOrganization financeOrganization : financeOrganizations) {
            if (financeOrganization instanceof Creditor) {
                Creditor creditor = (Creditor) financeOrganization;
                float loanInterestInOrganization = creditor.giveCredit(amount);
//                System.out.println("Проценты по кредиту составят: " + loanInterestInOrganization);
                if (loanInterestInOrganization == 0) {
                    continue;
                }
                if (loanInterestInOrganization < loanInterest) {
                    loanInterest = loanInterestInOrganization;
                    bestOrganizationToCredit = financeOrganization;
                }
            }
        }
        if (bestOrganizationToCredit != null)
            System.out.println(String.format("%s, используя нашу организацию, вы заплатите наименьшие проценты по кредиту, что в переводе на сумму составит: %.2f гривен", bestOrganizationToCredit.toString(), loanInterest));

    }

    //Депозит
    public static void findAndPrintBestOrganizationToDeposit(int amount, int depositPeriod) {
        List<FinanceOrganization> financeOrganizations = new Generator().generate();
        float maxMoneyAfterDeposite = 0;
        FinanceOrganization bestOrganizationToDeposit = null;
        for (FinanceOrganization financeOrganization : financeOrganizations) {
            if (financeOrganization instanceof Deposite) {
                Deposite deposite = (Deposite) financeOrganization;
                float moneyAfterDepositeInOrganization = deposite.investMoney(amount, depositPeriod);
//                System.out.println("проценты по депозиту составят: " + moneyAfterDepositeInOrganization);
                if (moneyAfterDepositeInOrganization > maxMoneyAfterDeposite) {
                    maxMoneyAfterDeposite = moneyAfterDepositeInOrganization;
                    bestOrganizationToDeposit = financeOrganization;
                }
            }
        }
        if (bestOrganizationToDeposit != null)
            System.out.println(String.format("%s. Воспользуясь услугами нашей организации, Вы получите максимальную выгоду по депозиту, что в переводе на сумму составит: %.2f гривен", bestOrganizationToDeposit.toString(), maxMoneyAfterDeposite));
    }

    // перевод денег
    public static void findAndPrintBestOrganizationToSendMoney(int amount) {
        List<FinanceOrganization> financeOrganizations = new Generator().generate();
        float serviceCoast = Float.MAX_VALUE;
        FinanceOrganization bestOrganizationToSendMoney = null;
        for (FinanceOrganization financeOrganization : financeOrganizations) {
            if (financeOrganization instanceof Sender) {
                Sender sender = (Sender) financeOrganization;
                float serviceBySendingMoneyCoastInOrganization = sender.sendMoney(amount);
//                System.out.println("Комиссия за услугу отправки денег составит: " + serviceBySendingMoneyCoastInOrganization + financeOrganization.toString());
                if (serviceBySendingMoneyCoastInOrganization < serviceCoast) {
                    serviceCoast = serviceBySendingMoneyCoastInOrganization;
                    bestOrganizationToSendMoney = financeOrganization;
                }
            }
        }
        if (bestOrganizationToSendMoney != null)
            System.out.println(String.format("%s. Воспользуясь услугами нашей организации, Вы заплатите минимальную комиссию, что в переводе на сумму составит: %.2f гривен", bestOrganizationToSendMoney.toString(), serviceCoast));
    }
}