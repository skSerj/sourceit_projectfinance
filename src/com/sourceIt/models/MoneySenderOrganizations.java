package com.sourceIt.models;

import com.sourceIt.interfaces.Sender;
import com.sourceIt.models.financialLogistics.FinanceOrganization;

public class MoneySenderOrganizations extends FinanceOrganization implements Sender {
    private String typeOfSendingOrganization;
    private float comissionBySendMoney;

    public MoneySenderOrganizations(String name, String address, String typeOfSendingOrganization, float comissionBySendMoney) {
        super(name, address);
        this.typeOfSendingOrganization = typeOfSendingOrganization;
        this.comissionBySendMoney = comissionBySendMoney;
    }


    @Override
    public float sendMoney(int amount) {
        return amount * comissionBySendMoney;
    }
}
