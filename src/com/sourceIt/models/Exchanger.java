package com.sourceIt.models;

import com.sourceIt.models.financialLogistics.FinanceOrganization;
import com.sourceIt.interfaces.MoneyChanger;
import com.sourceIt.models.financialLogistics.VerificationLimits;

import java.util.List;

public class Exchanger extends FinanceOrganization implements MoneyChanger {
    private Currency[] rates;

    public Exchanger(String name, String address, List<Currency> rates) {
        super(name, address);
        this.rates = rates.toArray(new Currency[0]);
    }

    public Currency[] getRates() {
        return rates;
    }

    @Override
    public float convertMoney(int amount, String desiredCurrencyName, String desiredOperationWithCurrency) {
        float moneyAfterConvertation = 0;
        for (Currency rate : rates) {
            if (rate.getName().equalsIgnoreCase(desiredCurrencyName) && (desiredOperationWithCurrency.equalsIgnoreCase(buyCurrency))) {
                moneyAfterConvertation = amount / rate.getRateOfSale();
                break;
            } else if (rate.getName().equalsIgnoreCase(desiredCurrencyName) && (desiredOperationWithCurrency.equalsIgnoreCase(sellCurrency))) {
                moneyAfterConvertation = amount * rate.getRateOfPurchase();
                break;
            }
        }
        return moneyAfterConvertation;
    }
}


