package com.sourceIt.models.financialLogistics;

public class VerificationLimits {


    public static boolean isOverLimitToChangeInBank(int amount, int maxAmouttoChange) {
        return amount < maxAmouttoChange;
    }

    public static boolean isOverLimitToCreditInBank(int amount, float maxMoneyToCredit) {
        return amount < maxMoneyToCredit;
    }

    public static boolean isOverMaxDepositPeriodInBank(int depositPeriod, int maxDepositPeriod) {
        return depositPeriod < maxDepositPeriod + 1;
    }

    public static boolean isOverLimitToCreditInCreditOrganization(int amount, float maxMoneyToCredit) {
        return amount < maxMoneyToCredit;
    }

    public static boolean isOverDepositPeriodInInvestmentFund(int depositPeriod, int minDepositPeriod) {
        return depositPeriod > minDepositPeriod - 1;
    }
}


