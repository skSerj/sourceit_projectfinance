package com.sourceIt.models;

import com.sourceIt.interfaces.Creditor;
import com.sourceIt.models.financialLogistics.FinanceOrganization;
import com.sourceIt.models.financialLogistics.VerificationLimits;

public class CreditOrganizations extends FinanceOrganization implements Creditor {
    private String typeOfCreditOrganization;
    private float maxMoneyToCredit;
    private float creditInterestRate;

    public CreditOrganizations(String name, String address, String typeCretitOrganization, float maxMoneyToCredit, float creditInterestRate) {
        super(name, address);
        this.typeOfCreditOrganization = typeCretitOrganization;
        this.maxMoneyToCredit = maxMoneyToCredit;
        this.creditInterestRate = creditInterestRate;
    }

    @Override
    public float giveCredit(int amount) {
        if (VerificationLimits.isOverLimitToCreditInCreditOrganization(amount, maxMoneyToCredit)) {
            return amount * creditInterestRate;
        }
        return 0;
    }
}
