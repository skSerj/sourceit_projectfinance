package com.sourceIt.data;

import com.sourceIt.models.*;
import com.sourceIt.models.financialLogistics.FinanceOrganization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Generator {
    public Generator() {
    }

    public List<FinanceOrganization> generate() {
        List<Currency> currenciesInBank = new ArrayList<>();
        currenciesInBank.add(new Currency("usd", 27.35F, 27.90F));
        currenciesInBank.add(new Currency("eur", 29.65F, 30.50F));
        currenciesInBank.add(new Currency("rub", 0.320F, 0.360F));

        List<Currency> currenciesInExchanger = new ArrayList<>();
        currenciesInExchanger.add(new Currency("usd", 27.20F, 28.00F));
        currenciesInExchanger.add(new Currency("eur", 29.30F, 30.90F));
        currenciesInExchanger.add(new Currency("rub", 0.300F, 0.370F));

        List<FinanceOrganization> financeOrganizations = new ArrayList<>();
        financeOrganizations.add(new Bank("ПриватБанк", "г.Харьков, ул.Гвардейцев-Широнинцев, д.45", 2005, currenciesInBank, 150001, 15, 200001, 0.25f, 12, 0.1f, 0.01f, 25));
        financeOrganizations.add(new Exchanger("BestChanger", "г.Харьков, ул.Валентиновская, 27", currenciesInExchanger));
        financeOrganizations.add(new CreditOrganizations("ломбард Благо", "г.Харьков, ул.Валентиновская, 37", "Ломбард", 50001, 0.40f));
        financeOrganizations.add(new CreditOrganizations("Кредитна спілка Самопоміч", "м.Харків, вул.Академіка Павлова, 148А", "Кредитный союз", 100001, 0.2f));
        financeOrganizations.add(new CreditOrganizations("КредитКафе", "г.Харьков, пр-т Тракторостроителей, 128-В", "КредитКафе", 4001, 4.0f));
        financeOrganizations.add(new InvestmentFund("Инвестиционный фонд Круар", "г.Харьков, ул.Военная, 37", 12, 0.11f));
        financeOrganizations.add(new MoneySenderOrganizations("Новая почта", "г. Харьков, просп. Тракторосторителей, 89б", "почта", 0.02f));
        financeOrganizations.add(new MoneySenderOrganizations("Сервис онлайн переводов Ipay", "https://www.ipay.ua/", "интернет-перевод", 0.002f));

        return financeOrganizations;
    }
}
