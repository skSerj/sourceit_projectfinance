package com.sourceIt.interfaces;

public interface Deposite {

    float investMoney(int amount, int depositPeriod);
}
