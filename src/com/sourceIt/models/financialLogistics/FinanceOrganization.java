package com.sourceIt.models.financialLogistics;

public class FinanceOrganization {
    private String name;
    private String address;

    @Override
    public String toString() {
        return String.format("К вашим услугам: %s, расположенный по адресу: %s", name, address);
    }

    public FinanceOrganization(String name, String address) {
        this.name = name;
        this.address = address;


    }
}



