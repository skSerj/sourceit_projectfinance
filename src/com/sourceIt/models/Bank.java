package com.sourceIt.models;

import com.sourceIt.models.financialLogistics.FinanceOrganization;
import com.sourceIt.interfaces.Creditor;
import com.sourceIt.interfaces.Deposite;
import com.sourceIt.interfaces.MoneyChanger;
import com.sourceIt.interfaces.Sender;
import com.sourceIt.models.financialLogistics.VerificationLimits;

import java.util.List;

public class Bank extends FinanceOrganization implements MoneyChanger, Creditor, Deposite, Sender {

    private int yearOfLicense;
    private Currency[] rates;
    private int maxAmouttoChange;
    private int exchangeComission;
    private float maxMoneyToCredit;
    private float creditInterestRate;
    private int maxDepositPeriod;
    private float depositePerсent;
    private float comissionBySendMoney;
    private float costSendMoney;

    public Bank(String name, String address, int yearOfLicense, List<Currency> rates, int maxAmouttoChange, int exchangeComission, float maxMoneyToCredit, float creditInterestRate, int maxDepositPeriod, float depositePerсent, float comissionBySendMoney, float costSendMoney) {
        super(name, address);
        this.yearOfLicense = yearOfLicense;
        this.rates = rates.toArray(new Currency[0]);
        this.maxAmouttoChange = maxAmouttoChange;
        this.exchangeComission = exchangeComission;
        this.maxMoneyToCredit = maxMoneyToCredit;
        this.creditInterestRate = creditInterestRate;
        this.maxDepositPeriod = maxDepositPeriod;
        this.depositePerсent = depositePerсent;
        this.comissionBySendMoney = comissionBySendMoney;
        this.costSendMoney = costSendMoney;
    }

    public Currency[] getRates() {
        return rates;
    }

    @Override
    public float convertMoney(int amount, String desiredCurrencyName, String desiredOperationWithCurrency) {
        if (VerificationLimits.isOverLimitToChangeInBank(amount, maxAmouttoChange)) {
            for (Currency rate : rates) {
                if (rate.getName().equalsIgnoreCase(desiredCurrencyName) && (desiredOperationWithCurrency.equalsIgnoreCase(buyCurrency))) {
                    return (amount - exchangeComission) / rate.getRateOfSale();
                } else if (rate.getName().equalsIgnoreCase(desiredCurrencyName) && (desiredOperationWithCurrency.equalsIgnoreCase(sellCurrency))) {
                    return (amount * rate.getRateOfPurchase()) - exchangeComission;
                }
            }
        }
        return 0;
    }

    @Override
    public float giveCredit(int amount) {
        if (VerificationLimits.isOverLimitToCreditInBank(amount, maxMoneyToCredit)) {
            return amount * creditInterestRate;
        }
        return 0;
    }

    @Override
    public float investMoney(int amount, int depositPeriod) {
        if (VerificationLimits.isOverMaxDepositPeriodInBank(depositPeriod, maxDepositPeriod)) {
            return amount * depositePerсent;
        }
        return 0;
    }

    @Override
    public float sendMoney(int amount) {
        return amount * comissionBySendMoney + costSendMoney;
    }
}




