package com.sourceIt.interfaces;

public interface Creditor {
    float giveCredit(int amount);
}
