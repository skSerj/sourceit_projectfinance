package com.sourceIt.models;

import com.sourceIt.models.financialLogistics.FinanceOrganization;
import com.sourceIt.interfaces.Deposite;
import com.sourceIt.models.financialLogistics.VerificationLimits;

public class InvestmentFund extends FinanceOrganization implements Deposite {
    private int minDepositPeriod;
    private float depositePerсent;

    public InvestmentFund(String name, String address, int minDepositPeriod, float depositePerсent) {
        super(name, address);
        this.minDepositPeriod = minDepositPeriod;
        this.depositePerсent = depositePerсent;
    }

    @Override
    public float investMoney(int amount, int depositPeriod) {
        if (VerificationLimits.isOverDepositPeriodInInvestmentFund(depositPeriod, minDepositPeriod)) {
            return amount * depositePerсent;
        }
        return 0;
    }
}

